# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: acloos <acloos@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/05/10 10:35:01 by acloos            #+#    #+#              #
#    Updated: 2023/01/19 08:42:31 by acloos           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SERVER					=	server
CLIENT					=	client
CC						=	gcc
CFLAGS					=	-Wall -Wextra -Werror -g3
RM						=	rm -rf

#mandatory
SRCBAZ_SE				=	server utils
SRC_SE						=	$(addsuffix .c, $(addprefix sources/, $(SRCBAZ_SE)))

SRCBAZ_CL				=	client utils
SRC_CL						=	$(addsuffix .c, $(addprefix sources/, $(SRCBAZ_CL)))

OBJSE_DIR					=	obj_se
OBJ_SE						=	$(SRC_SE:sources/%.c=$(OBJSE_DIR)/%.o)

OBJCL_DIR					=	obj_cl
OBJ_CL						=	$(SRC_CL:sources/%.c=$(OBJCL_DIR)/%.o)

#bonus
SRCBAZ_SEB				=	server utils
SRC_SEB						=	$(addsuffix _bonus.c, $(addprefix bonus/, $(SRCBAZ_SE)))

SRCBAZ_CLB				=	client utils
SRC_CLB						=	$(addsuffix _bonus.c, $(addprefix bonus/, $(SRCBAZ_CL)))


OBJSEB_DIR					=	obj_seb
OBJ_SEB						=	$(SRC_SEB:bonus/%.c=$(OBJSEB_DIR)/%.o)

OBJCLB_DIR					=	obj_clb
OBJ_CLB						=	$(SRC_CLB:bonus/%.c=$(OBJCLB_DIR)/%.o)

LIBFT_PATH				=	./libft
LIBFT					=	$(LIBFT_PATH)/libft.a
LIBINCL					=	-L libft/ -lft

$(OBJSE_DIR)/%.o:			sources/%.c
						$(CC) $(CFLAGS) -I libft/sources -I sources  -c $< -o $@

$(OBJCL_DIR)/%.o:			sources/%.c
						$(CC) $(CFLAGS) -I libft/sources -I sources  -c $< -o $@

$(OBJSEB_DIR)/%.o:			bonus/%.c
						$(CC) $(CFLAGS) -I libft/sources -I bonus  -c $< -o $@

$(OBJCLB_DIR)/%.o:			bonus/%.c
						$(CC) $(CFLAGS) -I libft/sources -I bonus  -c $< -o $@


all:					$(SERVER) $(CLIENT)
						@echo "\033[32m[Minitalk is now ready]\033[0m"

bonus: 					bonus_s bonus_c
						@echo "\033[35m[Minitalk + bonus is ready]\033[0m"

$(SERVER):				$(LIBFT) $(OBJSE_DIR) $(OBJ_SE)
						$(CC) $(CFLAGS) $(OBJ_SE) -o $(SERVER) $(LIBINCL)
						@echo "\033[32m[Server created]\033[0m"

$(CLIENT):				$(LIBFT) $(OBJCL_DIR) $(OBJ_CL)
						$(CC) $(CFLAGS) $(OBJ_CL) -o $(CLIENT) $(LIBINCL)
						@echo "\033[32m[Client created]\033[0m"

$(LIBFT):
						$(MAKE) -C $(LIBFT_PATH) all -s
						@echo "\033[32m[Libft created]\033[0m"

$(OBJSE_DIR):
						mkdir -p $(OBJSE_DIR)

$(OBJCL_DIR):
						mkdir -p $(OBJCL_DIR)


bonus_s:				$(LIBFT) $(OBJSEB_DIR) $(OBJ_SEB)
						$(CC) $(CFLAGS) $(OBJ_SEB) -o $(SERVER) $(LIBINCL)
						@echo "\033[35m[Server bonus created]\033[0m"

bonus_c:				$(LIBFT) $(OBJCLB_DIR) $(OBJ_CLB)
						$(CC) $(CFLAGS) $(OBJ_CLB) -o $(CLIENT) $(LIBINCL)
						@echo "\033[35m[Client bonus created]\033[0m"


$(OBJSEB_DIR):
						mkdir -p $(OBJSEB_DIR)

$(OBJCLB_DIR):
						mkdir -p $(OBJCLB_DIR)


$(OBJ_BONUS_DIR):
						mkdir -p $(OBJ_BONUS_DIR)

clean:
						$(MAKE) -C $(LIBFT_PATH) clean
						$(RM) $(OBJSE_DIR) $(OBJCL_DIR) $(OBJSEB_DIR) $(OBJCLB_DIR)
						@echo "\033[33m[Cleaned up]\033[0m"

fclean:					clean
						$(MAKE) -C $(LIBFT_PATH) fclean
						$(RM) $(SERVER) $(CLIENT)
						@echo "\033[33m[Fully cleaned up]\033[0m"

re:						fclean all

.PHONY:	all clean fclean re bonus libft bonus_s bonus_c
