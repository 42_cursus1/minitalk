/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:51:22 by acloos            #+#    #+#             */
/*   Updated: 2023/01/20 14:21:59 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

void	send_byte(pid_t server_pid, int byte)
{
	int	nb;
	int	power;
	int	div;

	(void)server_pid;
	nb = 0;
	power = 10000000;
	div = 128;
	while (power > 0)
	{
		nb += (byte / div) * power;
		if ((byte / div) == 0)
			kill(server_pid, SIGUSR1);
		else
			kill(server_pid, SIGUSR2);
		pause();
		byte = byte % div;
		div /= 2;
		power /= 10;
	}
}

void	send_string(pid_t server_pid, char *mini_chat)
{
	int	i;

	i = 0;
	while (mini_chat[i] != '\0')
	{
		send_byte(server_pid, mini_chat[i]);
		i++;
		usleep(450);
	}
	send_byte(server_pid, '\0');
}

static void	ft_handler(int sig)
{
	if (sig == SIGUSR2)
	{
		ft_printf("%s%sServer received the full message !%s\n", BOLD,
			F_D_GREEN, RST);
	}
	if (sig == SIGUSR1)
	{
		ft_printf("\t%s%s%sServer is receiving...%s\n",
			DIM, ITALICS, F_L_GREEN, RST);
		usleep(100);
	}
}

int	main(int argc, char **argv)
{
	signal(SIGUSR1, ft_handler);
	signal(SIGUSR2, ft_handler);
	if (argc != 3)
	{
		ft_printf("%s%sSyntax error detected !%s\n", BOLD, F_D_RED, RST);
		ft_printf("%s%sPlease use as :%s\n", BOLD, F_L_YELLOW, RST);
		ft_printf("\t%s%s%s./client ", BOLD, F_L_GREEN, B_DEFAULT);
		ft_printf("<server PID> <your input> %s", RST);
		ft_printf("%s \n", RST);
		return (EXIT_FAILURE);
	}
	else if (kill(ft_atoi(argv[1]), 0) < 0)
		ft_error("This PID does not exist !\nPlease enter valid PID");
	else
		send_string(ft_atoi(argv[1]), argv[2]);
	return (0);
}

/* int	main(int argc, char **argv)
{
	signal(SIGUSR1, ft_handler);
	if (argc != 3)
	{
		ft_printf("\033[1m\033[31mSyntax error detected !\n\033[0m");
		ft_printf("\033[1m\033[93mPlease use as :\033[0m\n");
		ft_printf("\t\033[1m\033[92m\033[49m./client ");
		ft_printf("<server PID> <your input> \033[0m");
		ft_printf("\033[0m \n");
		return (1);
	}
	else if (kill(ft_atoi(argv[1]), 0) < 0)
	{
		ft_printf("\033[1m\033[31m\033[43mPID error ! \n");
		ft_printf("Please enter valid PID \033[0m");
		return (1);
	}
	else
		send_string(ft_atoi(argv[1]), argv[2]);
	return (0);
} */
