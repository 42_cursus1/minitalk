/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:51:41 by acloos            #+#    #+#             */
/*   Updated: 2023/01/20 07:56:45 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

void	check_pid(void)
{
	pid_t	server_pid;

	server_pid = getpid();
	if (server_pid > 1)
	{
		ft_printf("\n");
		ft_printf("\t %s%s%s          _       _       _        _ _ \n", F_D_GREEN,
			BOLD, SLOW_BLINK);
		ft_printf("\t%s _ ___ __ (_)_ __ (_)     | |_ __ _| | | __ \n", F_D_RED);
		ft_printf("\t%s|  _  |_ \\| | |_ \\| | ___ | __/ _| | | |/ / \n",
			F_D_YELLOW);
		ft_printf("\t%s| | | | | | | | | | | ___ | || |_| | |   < \n",
			F_D_MAGENTA);
		ft_printf("\t%s|_| |_| |_|_|_| |_|_|      \\_\\\\__|_|_|_||_\\ \n", F_D_BLUE);
		ft_printf("\n%s%s%s%s", OFF_BLINKS, F_D_GREEN, DIM, ITALICS);
		ft_printf("%54s", "a mini-program by acloos\n\n\n");
		ft_printf("%s%s%s\tServer PID : %d\t\t\n\n%s", RST, BOLD, F_D_GREEN,
			server_pid, RST);
		ft_printf("\n");
	}
	else
		ft_error("Could not launch the program o_O ");
}

void	get_sig(int siggy, pid_t client_pid)
{
	static int	nb;
	static char	byte[9];

	if (!nb)
		nb = 0;
	byte[nb] = siggy;
	nb++;
	if (nb == 8)
	{
		byte[9] = '\0';
		get_msg(byte, client_pid);
		bzero(byte, 9);
		nb = 0;
	}
}

void	setting_sigaction(struct sigaction *act)
{
	if (sigaction(SIGUSR1, act, NULL) < 0)
		ft_error("Problem with SIGUSR1");
	if (sigaction(SIGUSR2, act, NULL) < 0)
		ft_error("Problem with SIGUSR2");
}

void	ft_handler(int sig, siginfo_t *info, void *context)
{
	pid_t	client_pid;

	(void)context;
	client_pid = info->si_pid;
	if (sig == SIGUSR1)
		get_sig('0', client_pid);
	else if (sig == SIGUSR2)
		get_sig('1', client_pid);
	usleep(450);
	kill(client_pid, SIGUSR1);
}

int	main(void)
{
	struct sigaction	act;

	check_pid();
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGUSR1);
	sigaddset(&act.sa_mask, SIGUSR2);
	act.sa_sigaction = ft_handler;
	act.sa_flags = SA_SIGINFO | SA_RESTART;
	setting_sigaction(&act);
	while (1)
		pause();
	return (0);
}
