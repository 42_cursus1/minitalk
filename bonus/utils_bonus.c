/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/11 10:32:19 by acloos            #+#    #+#             */
/*   Updated: 2023/01/20 12:05:29 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk_bonus.h"

char	*g_msg;

void	ft_error(char *str)
{
	ft_printf("%s%s%sError detected:%s\n", BOLD, F_D_RED, SLOW_BLINK, RST);
	ft_printf("%s%s%s", F_L_YELLOW, ITALICS, str);
	ft_printf("%s\n", RST);
	exit(EXIT_FAILURE);
}

char	get_byte(char *byte)
{
	int	i;
	int	res;

	i = 0;
	res = 0;
	while (i < 8)
	{
		res = res * 2 + (byte[i] - 48);
		i++;
	}
	return (res);
}

char	*join_msg(char *g_msg, char *car)
{
	char	*tmp;

	tmp = g_msg;
	g_msg = ft_strjoin(tmp, car);
	if (!g_msg)
		ft_error("Could not malloc joined message");
	free(tmp);
	return (g_msg);
}

void	ft_printing(char *g_msg)
{
	ft_printf("\n\tPrinting message from client :\n");
	if (g_msg == NULL)
		ft_printf("\t\t%s%sTrying to trick me with a NULL string ?!%s",
			BOLD, F_D_RED, RST);
	else
		ft_printf("%s", g_msg);
}

void	get_msg(char *byte, pid_t client_pid)
{
	char		car[2];

	car[0] = get_byte(byte);
	car[1] = '\0';
	if (car[0] == '\0')
	{
		ft_printing(g_msg);
		free(g_msg);
		g_msg = NULL;
		kill(client_pid, SIGUSR2);
	}
	else if (!g_msg)
	{
		g_msg = ft_strdup(car);
		if (!g_msg)
			ft_error("Could not malloc new message");
		kill(client_pid, SIGUSR1);
	}
	else
	{
		g_msg = join_msg(g_msg, car);
	}
}

/* void	pid_printer(int got_pid)
{
	ft_printf("\n");
	ft_printf("\t %s%s%s          _       _       _        _ _ \n", F_D_GREEN, 
	BOLD, SLOW_BLINK);
	ft_printf("\t%s _ ___ __ (_)_ __ (_)     | |_ __ _| | | __ \n", F_D_RED);
	ft_printf("\t%s|  _  |_ \\| | |_ \\| | ___ | __/ _| | | |/ / \n", F_D_YELLOW);
	ft_printf("\t%s| | | | | | | | | | | ___ | || |_| | |   < \n", F_D_MAGENTA);
	ft_printf("\t%s|_| |_| |_|_|_| |_|_|      \\_\\\\__|_|_|_||_\\ \n", F_D_BLUE);
	ft_printf("\n%s%s%s%s", OFF_BLINKS, F_D_GREEN, DIM, ITALICS);
	ft_printf("%54s", "a mini-program by acloos\n\n\n");
	ft_printf("%s%s%s\tServer PID : %d\t\t\n\n%s",RST, BOLD, F_D_GREEN, got_pid, 
	RST);
	ft_printf("\n");
} */

/* void	pid_printer(int got_pid)
{
	ft_printf("%s%s\n", F_D_GREEN, BOLD);
	ft_printf("%s  __    _  _  _   _  _       ____  __    _    _  _\n", F_D_RED);
	ft_printf("%s |  \\  / || || \\ | || | ___ |_  _||  \\  | |  | |//", 
	F_D_YELLOW);
	ft_printf("\n");
	ft_printf("%s |   \\/  || ||  \\| || ||___|  | | |   \\ | |_ |   \\ ", 
	F_D_MAGENTA);
	ft_printf("\n");
	ft_printf("%s |_/|_/|_||_||_/|__||_|       |_| |_|\\_\\|___||_|\\_\\", 
	F_D_BLUE);
	ft_printf("\n");
	ft_printf("\n");
	ft_printf("\n");
	ft_printf("\n");
	ft_printf("%s%s%51s\n\n\n", F_D_GREEN, DIM, "a mini-program by acloos");
	ft_printf("%s%s%s\tServer PID : %d\t\t\n\n%s", RST, BOLD, F_D_GREEN, got_pid, 
	RST);
} */
