/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk_bonus.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:51:36 by acloos            #+#    #+#             */
/*   Updated: 2023/01/19 15:34:18 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINITALK_BONUS_H
# define MINITALK_BONUS_H

# include<signal.h>
# include<unistd.h>
# include<stdlib.h>
# include<sys/types.h>
# include "libft.h"
# include "ansi_colors_bonus.h"

extern char	*g_msg;

void	ft_error(char *str);
void	get_sig(int siggy, pid_t client_pid);
void	get_msg(char *byte, pid_t client_pid);

#endif
